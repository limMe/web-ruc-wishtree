var domain = 'http://rucgirlsday.leanapp.cn/';
var storage = window.localStorage; 


//获取网页参数，根据参数决定加载哪个panel
function route(){
    var panelStr = getQueryString('panel');
    switch(panelStr){
        case 'title': showTitlePanel();break;
        case 'info': showInfoPanel();break;
        case 'login': showLoginPanel();break;
        case 'phone': showPhonePanel();break;
        case 'dashboard': showDashboard();break;
    }
}

//Panel展示
function hideOtherPanels(){
    var panels = document.getElementsByClassName('Panel');
    for(var i=0; i<panels.length; i++){
        panels[i].style.display = 'none';
    }
}

var meteorAnimationToken;
function showTitlePanel(){
    hideOtherPanels();
    document.getElementById('background-cover').style.opacity = '0';
    document.getElementById('titlePanel').style.display = 'block';
    
    //展示流星
    document.getElementById('meteor').style.display = 'block';
    meteorAnimationToken = setInterval(refreshMeteor,50); //我已经不止一次多加一个括号然后被这个回调语法搞得fucked up了
}

var leavingAnimationToken;
var leaveToWhere = 0; //0表示去介绍页，1表示去登陆页
function leaveTitlePanel(){
    document.getElementById('meteor').style.display = 'none';
    clearInterval(meteorAnimationToken);
    leavingAnimationToken = setInterval(steplyLeaveTitlePanel,100);
}

function showInfoPanel(){
    hideOtherPanels();
    document.getElementById('infoPanel').style.display = 'block';
    document.getElementById('infoPanel').style.height = window.innerHeight.toString() + 'px';
}

function showLoginPanel(){
    if(storage.getItem('access_token') != null && storage.getItem('access_token') != 'null'){
        showDashboard();
        return;
    }
    hideOtherPanels();
    document.getElementById('loginPanel').style.display = 'block';
}

function showPhonePanel(){
    hideOtherPanels();
    document.getElementById('phonePanel').style.display = 'block';
}

var access_token;
var duties = new Array();
var wishes = new Array();
function showDashboard(){
    hideOtherPanels();
    
    //check access_token
    if(storage.getItem('access_token') != null && storage.getItem('access_token') != 'null' ){
        document.getElementById('dashboardPanel').style.display = 'block';
        access_token = storage.getItem('access_token');
    } else {
        showLoginPanel();
        return;
    }
    
    document.getElementById('dashboardInfo').innerHTML = '加载中...';
    document.getElementById('wishList').innerHTML = '';
    document.getElementById('dutyList').innerHTML = '';
    
    var xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = function(){
        if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){    
            var gotJson = JSON.parse(xmlHttpRequest.responseText);
            if(gotJson.status == true){

                if(gotJson.gender == 'male'){
                    document.getElementById('dashboardInfo').innerHTML = '以下专属愿望需要您的实现噢:';
                    document.getElementById('dashboardDutyInfo').innerHTML = '以下是您已经许诺需要实现的愿望:';
                    
                    if(gotJson.wishes.length != 0){
                        for(var i=0; i<gotJson.wishes.length; i++){
                            var newWish = document.createElement('div');
                            newWish.setAttribute('class','infoBtnContainer');
                            
                            var status;
                            switch(gotJson.wishes[i].isadopted){
                                case true: status='已接受';break;
                                case false: status='未接受';break;
                            }
                            
                            newWish.innerHTML = '<a ontouchstart="fingerOnInfoBtn(\'wishBtn' + i.toString() + '\')" ontouchend="tapWishBtn(\''+i.toString()+'\')" id="wishBtn'+i.toString()+'" class="infoBtn"><div><strong>'+status+':</strong>'+gotJson.wishes[i].title+'<p class="hiddenWishID" id="hiddenWish'+i.toString()+'">'+gotJson.wishes[i].wishid+'</p></div></a>';
                            document.getElementById('wishList').appendChild(newWish);
                            
                            wishes.push(gotJson.wishes[i]);
                        }
                    }else{
                        var newWish = document.createElement('div');
                        newWish.setAttribute('class','infoBtnContainer');
                        newWish.innerHTML = '<a ontouchstart="fingerOnInfoBtn(\'wishBtn' + i.toString() + '\')" ontouchend="tapWishBtn(\''+i.toString()+'\')" id="wishBtn'+i.toString()+'" class="infoBtn"><div style="text-align:center">没有内容，但寂寞只是暂时的<p class="hiddenWishID"></p></div></a>';
                        document.getElementById('wishList').appendChild(newWish);
                    }
                    
                    if (gotJson.duties.length != 0) {
                        
                        for(var i=0; i<gotJson.duties.length; i++){
                        
                            var status;
                            switch(gotJson.duties[i].isdone){
                                case true: status='已完成';break;
                                case false: status='进行中';break;
                            }
                        
                            var newDuty = document.createElement('div');
                            newDuty.setAttribute('class','infoBtnContainer');
                            newDuty.innerHTML = '<a ontouchstart="fingerOnInfoBtn(\'dutyBtn' + i.toString() + '\')" ontouchend="tapDutyBtn(\''+i.toString()+'\')" id="dutyBtn'+i.toString()+'" class="infoBtn"><div ><strong>'+status+':</strong>'+gotJson.duties[i].wishtitle+'<p class="hiddenWishID" id="hiddenDuty'+i.toString()+'">'+gotJson.duties[i].wishid+'</p></div></a>';
                            document.getElementById('dutyList').appendChild(newDuty);
                            
                            duties.push(gotJson.duties[i]);
                        }
                    } else {
                        var newDuty = document.createElement('div');
                        newDuty.setAttribute('class','infoBtnContainer');
                        newDuty.innerHTML = '<a ontouchstart="fingerOnInfoBtn(\'dutyBtn' + i.toString() + '\')" ontouchend="tapDutyBtn(\''+i.toString()+'\')" id="dutyBtn'+i.toString()+'" class="infoBtn"><div style="text-align:center">还没有许诺。记得，赠人玫瑰，手留余香。<p class="hiddenWishID" ></p></div></a>';
                        document.getElementById('dutyList').appendChild(newDuty);
                    }
                    
                }
                
            }else{
                
            }
        }  
    };
    xmlHttpRequest.open('GET',domain + 'personDashboard?access_token=' + access_token,true);
    xmlHttpRequest.send();
 
}

function showSingleWishPanel(iswish, wishid){
    hideOtherPanels();
    document.getElementById('singleWishPanel').style.display = 'block';
    
    var target = new Object();
    for(var i=0; i<wishes.length; i++){
        if (wishes[i].wishid == wishid) {
            target.wish = wishes[i];
        }
    }
    for(var i=0; i<duties.length; i++){
        if (duties[i].wishid == wishid) {
            target.duty = duties[i];
        }
    }
    
    if(target.wish == undefined){
        target.wish = new Object();
    }
    if(target.duty == undefined){
        target.duty = new Object();
    }
    
    var girlname;
    if(target.duty.girlname != null && target.duty.girlname != undefined){girlname = target.duty.girlname;}
    else{girlname = '尚未揭穿';}
    
    var girlschool;
    if(target.wish.school != null && target.wish.school != undefined){girlschool = target.wish.school;}
    else{girlschool='尚未揭穿';}
    
    var boyname;
    if(target.wish.boyname != null && target.wish.boyname != undefined){boyname = target.wish.boyname;}
    else if(target.duty.boyname != null && target.duty.boyname != undefined){boyname = target.duty.boyname;}
    else{boyname = '尚未揭穿';}
    
    var boyschool;
    if(target.duty.school != null && target.duty.school != undefined){boyschool = target.duty.school;}
    else{boyschool = '尚未揭穿';}
    
    var girlgrade;
    if(target.wish.grade != null && target.wish.grade != undefined){girlgrade = translateGrade(target.wish.grade);}
    else{girlgrade = '未知';}
    
    var boygrade;
    if(target.duty.grade != null && target.duty.grade != undefined){boygrade = translateGrade(target.duty.grade);}
    else{boygrade = '未知';}
    
    var wishdetail;
    if(target.wish.detail != null && target.wish.detail != undefined){wishdetail = target.wish.detail;}
    else{wishdetail = '愿望内容略';}

    document.getElementById('girlname').innerHTML = '<strong>女&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;生: </strong>'+girlname;
    document.getElementById('girlschool').innerHTML = '<strong>女生学院: </strong>'+girlschool;  
    document.getElementById('boyname').innerHTML = '<strong>男&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;生: </strong>'+boyname;          
    document.getElementById('boyschool').innerHTML = '<strong>男生学院: </strong>'+boyschool;  
    document.getElementById('girlgrade').innerHTML = '<strong>女生年级: </strong>'+girlgrade;  
    document.getElementById('boygrade').innerHTML = '<strong>男生年级: </strong>'+boygrade;  
    document.getElementById('wishdetail').innerHTML = wishdetail;
    
    if (target.duty.wishtitle != undefined) {
        document.getElementById('singWishTitle').innerHTML = target.duty.wishtitle;
    } else {
        document.getElementById('singWishTitle').innerHTML = target.wish.title;
    }
    
}

//Title Panel
var meteorCount = 0;
function refreshMeteor(){
    meteorCount++;
    if(meteorCount > 40){
        meteorCount = 0;
    }
    document.getElementById('meteor').style.paddingTop = (20 + meteorCount*4).toString() + 'px';
    document.getElementById('meteor').style.paddingLeft = (300 - meteorCount*5).toString() + 'px'
    document.getElementById('meteor').style.opacity = (1 - meteorCount*0.02).toString();
}

var leavingCount = 0;
function steplyLeaveTitlePanel(){
    leavingCount++;
    if(leavingCount < 11){
        document.getElementById('titleZone').style.opacity = (1 - leavingCount*0.1).toString();
    }else if (leavingCount < 21){
        document.getElementById('background-cover').style.opacity = ((leavingCount - 10) * 0.1 ).toString();
    }else{
        leavingCount = 0;
        clearInterval(leavingAnimationToken);
        if (leaveToWhere == 1) {
            showLoginPanel();
        } else {
            showInfoPanel();
        }
    }
    
}

//Btn_tap
//点击活动介绍
function tapDescribeBtn() {
    var spot = document.getElementById('describeBtn_title').firstChild;
    spot.style.backgroundColor = '#8299a1';
    leaveToWhere = 0;
    leaveTitlePanel();
}

//点击立即参与
function tapLoginBtn() {
    var spot = document.getElementById('loginBtn_title').firstChild;
    leaveToWhere = 1;
    spot.style.backgroundColor = '#8299a1';
    leaveToWhere = 1;
    leaveTitlePanel();
}

//Info Panel
function tapInfoLoginBtn(){
    var spot = document.getElementById('infoLoginBtn_title').firstChild;
    spot.style.backgroundColor = '#8299a1';
    showLoginPanel();
}

//Login Panel
function tapFinalLoginBtn(){
    
    document.getElementById('loginAlert').style.display = 'none';
    
    var spot = document.getElementById('finalLoginBtn_title').firstChild;
    spot.style.backgroundColor = '#8299a1';
    
    var xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = function(){
        if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){    
            var gotJson = JSON.parse(xmlHttpRequest.responseText);
            if(gotJson.status == true){
                
                if(storage){
                    storage.setItem('access_token',gotJson.access_token);
                }
                    
                //不跳转到手机绑定
                if(gotJson.isbinded == true){
                    showDashboard();
                }
                
                //跳转手机绑定
                else{
                    showPhonePanel();
                }
            }else{
                document.getElementById('loginAlert').innerHTML = '登录失败，请检查您的输入';
                document.getElementById('loginAlert').style.display = 'block';
            }
        }  
    };
    xmlHttpRequest.open('POST',domain + 'login',true);
    xmlHttpRequest.setRequestHeader("Content-Type","application/x-www-form-urlencoded"); 
    var queryString = 'ssid='+document.getElementById('loginSSID').value + '&password=' + document.getElementById('loginPwd').value; 
    xmlHttpRequest.send(queryString);
}

//Phone Panel
function tapPhoneBtn(){
    var spot = document.getElementById('phoneBtn').firstChild;
    spot.style.backgroundColor = '#8299a1';
    
    document.getElementById('phoneAlert').innerHTML = '';
    document.getElementById('phoneAlert').style.display = 'none';
    
    if(document.getElementById('phoneInput').value == ''){
        document.getElementById('phoneAlert').innerHTML = '请填写手机号';
        document.getElementById('phoneAlert').style.display = 'block';
    }
    
    var xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = function(){
        if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){    
            var gotJson = JSON.parse(xmlHttpRequest.responseText);
            if(gotJson.status == true){
                showDashboard();
            }else{
                document.getElementById('phoneAlert').innerHTML = '绑定失败！';
                document.getElementById('phoneAlert').style.display = 'block';
            }
        }  
    };
                
    xmlHttpRequest.open('GET',domain + 'phonebinding?phonenum=' + document.getElementById('phoneInput').value +'&access_token=' + storage.getItem('access_token'),true);
    xmlHttpRequest.send();
}

function tapJumpBtn(){
    var spot = document.getElementById('jumpBtn').firstChild;
    spot.style.backgroundColor = '#8299a1';
    
    showDashboard();
}

//Dashboard Panel 
function tapWishBtn(num){
    var spot = document.getElementById('wishBtn'+num).parentNode;
    spot.style.opacity = '0.7';
    
    var wishid = document.getElementById('hiddenWish'+num).innerHTML;
    showSingleWishPanel(true,wishid);
}

function tapDutyBtn(num){
    var spot = document.getElementById('dutyBtn'+num).parentNode;
    spot.style.opacity = '0.7';
    
    var wishid = document.getElementById('hiddenDuty'+num).innerHTML;
    showSingleWishPanel(false,wishid);
}

function tapLogoffBtn(){
    var spot = document.getElementById('logoffBtn').firstChild;
    spot.style.backgroundColor = '#8299a1';
    
    storage.setItem('access_token',null);
    
    showTitlePanel();
}

//Single Wish Panel 
function tapBackBtn(){
    var spot = document.getElementById('backBtn').firstChild;
    spot.style.backgroundColor = '#8299a1';
    
    showDashboard();
}

//统一元素处理
//这类<a>元素 包含一个<div>元素
function fingerOnbtn(btnElement) {
    var spot = document.getElementById(btnElement).firstChild;
    spot.style.backgroundColor = '#26484a';
}

function fingerOnInfoBtn(btnElement) {
    var spot = document.getElementById(btnElement).parentNode;
    spot.style.opacity = '0.95';
}

//工具类
function getQueryString(name) {
    var result = location.search.match(new RegExp('[\?\&]' + name+ '=([^\&]+)','i')); 
    if(result == null || result.length < 1){ 
        return ''; 
    } 
    return result[1]; 
}

function translateGrade(pattern){
    
    switch(pattern){
        case "20152": return "本科一年级";
        case "20151": return "硕士一年级";
        case "20150": return "博士一年级";
        case "20142": return "本科二年级";
        case "20141": return "硕士二年级";
        case "20140": return "博士二年级";
        case "20132": return "本科三年级";
        case "20131": return "硕士三年级";
        case "20130": return "博士三年级";
        case "20122": return "本科四年级";
    }
}